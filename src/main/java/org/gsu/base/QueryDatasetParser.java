package org.gsu.base;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vijay.akkineni; berkay
 */
public class QueryDatasetParser {

	private String queryDataDir;
	private ArrayList<File> featureFiles;

	public Map<String, Map<Integer, Trajectory>> fileTrajectoryMap;

	public QueryDatasetParser() {

		queryDataDir = ConfigReader.getQueryDatasetDirectory();
		featureFiles = new ArrayList<File>();

		fileTrajectoryMap = new HashMap<String, Map<Integer, Trajectory>>();

		File datasetFolder = new File(queryDataDir);
		findFeatureFiles(datasetFolder);

		System.out.println("Query Dataset Files: " + listFeatureFiles());
		for (int i = 0; i < featureFiles.size(); i++) {
			readFeatureFile(featureFiles.get(i));
		}

	}

	public void findFeatureFiles(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				findFeatureFiles(fileEntry);
			} else {
				System.out.println(fileEntry.getName());
				if (!fileEntry.getName().contains("metadata") && !fileEntry.getName().contains(".DS_Store")) {
					featureFiles.add(fileEntry);
				}
			}
		}
	}

	public String listFeatureFiles() {
		String s = "";
		for (int i = 0; i < featureFiles.size(); i++) {
			s += featureFiles.get(i) + ", ";
		}
		return s;
	}

	public int readFeatureFile(File file) {

		Map<Integer, Trajectory> trajectoryMap = new HashMap<Integer, Trajectory>();

		file.getName();
		BufferedReader br;
		int count = 0;
		try {
			String line;
			br = new BufferedReader(new FileReader(file));

			while ((line = br.readLine()) != null) {

				String[] result = line.split("\\t", 3); // result[0]: instance
														// id
				// result[1]: timestamp
				// result[2]: geom
				Integer trajectoryID = new Integer(result[0]);
				Integer timestamp = new Integer(result[1]);
				WKTReader geomReader = new WKTReader();
				Geometry geom = null;
				try {
					geom = geomReader.read(result[2]);
				} catch (ParseException e) {
					System.out.println("Exception: geometry cannot be read (" + trajectoryID + "," + timestamp + ")");
					e.printStackTrace();
				}

				Trajectory trajectory = null;
				if ((trajectory = trajectoryMap.get(trajectoryID)) == null) { // then
																				// add
																				// a
																				// new
																				// instance
					count++;
					trajectory = new Trajectory(trajectoryID, null, null);
					trajectory.addGeometry(timestamp, geom);

					trajectoryMap.put(trajectoryID, trajectory);

				} else { // add a new geometry
					trajectoryMap.get(trajectoryID).addGeometry(timestamp, geom);
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			System.out.println("Exception: feature file cannot be found (" + file + ")");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Exception: feature file cannot be read (" + file + ")");
			e.printStackTrace();
		}

		fileTrajectoryMap.put(file.getName(), trajectoryMap);
		return count;
	}
}
