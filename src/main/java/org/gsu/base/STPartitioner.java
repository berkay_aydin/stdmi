package org.gsu.base;

import java.util.TreeMap;
import java.util.TreeSet;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

public class STPartitioner {

	public static int D1;
	public static int D2;

	public static int cell_x;
	public static int cell_y;

	public static int diff_t;

	public STPartitioner(){
		new ConfigReader();
		D1 = ConfigReader.getD1();
		D2 = ConfigReader.getD2();
		cell_x = ConfigReader.getX_partition();
		cell_y = ConfigReader.getY_partition();
		diff_t = ConfigReader.getTime_step();


		//System.out.println(D1 + " " + D2 + "\t" + cell_x + " " + cell_y + "\t" + diff_t);
	}

	public TreeSet<String> getPartitions(Trajectory i){
		TreeSet<String> st_partition_ids = new TreeSet<String>();

		//System.out.println(i);
		Long st = i.getStartTime();
		Long et = i.getEndTime();

		TreeMap<Long, Geometry> trajGeometries = i.getGeometryMap();
		Long partitionStart = st;
		for(Long time_partition = st/diff_t; time_partition <= et/diff_t; time_partition++){

			String timePartitionString = time_partition.toString() + "|";
			
			Long t = partitionStart;
			for(; t < diff_t*(1+(partitionStart/diff_t)) && t <= et; t++){
				Geometry g = trajGeometries.get(t);
				if(g != null && !g.isEmpty()){
					double min_x = trajGeometries.get(t).getEnvelopeInternal().getMinX();
					double max_x = trajGeometries.get(t).getEnvelopeInternal().getMaxX();

					double min_y = trajGeometries.get(t).getEnvelopeInternal().getMinY();
					double max_y = trajGeometries.get(t).getEnvelopeInternal().getMaxY();

					//from all cells spanning through min_x to max_x
					for( Integer xCell = (int) (min_x/cell_x); xCell <= (int)(max_x/cell_x); xCell++ ){
						//from all cells spanning through min_y to max_y
						for( Integer yCell = (int) (min_y/cell_y); yCell <= (int) (max_y/cell_y); yCell++ ){
							
							Envelope e = new Envelope(min_x, max_x, min_y, max_y);
							if(new GeometryFactory().toGeometry(e).intersects(g)){
								String spacePartitionString = xCell.toString() + ";" + yCell.toString(); 
								st_partition_ids.add(timePartitionString + spacePartitionString);
							}
						}
					}
				} else{
					if(g == null)
						System.out.println("Check " + t + "of\n" + i);
				}
			}
			partitionStart = t++;
		}
		return st_partition_ids;
	}

	/**
	 * Explores the partitions through which the trajectory (traj) spans
	 * Trajectory Segment objects (TSegment) are to be initialized empty with
	 * trajectory id, then the segment id. Segment id is same with partition identifier
	 * addGeometry() method of TSegment is used.
	 * 
	 * @param traj - trajectory to be explored
	 * @return a map of partition strings to segmented trajectories
	 */
	public TreeMap<String, TSegment> getSegments(Trajectory traj){
		TreeMap<String, TSegment> trajSegments = new TreeMap<>();

		//System.out.println(i);
		Long st = traj.getStartTime();
		Long et = traj.getEndTime();

		TreeMap<Long, Geometry> trajGeometries = traj.getGeometryMap();
		Long partitionStart = st;
		for(Long time_partition = st/diff_t; time_partition <= et/diff_t; time_partition++){

			String timePartitionString = time_partition.toString() + "|";
			
			Long t = partitionStart;
			for(; t < diff_t*(1+(partitionStart/diff_t)) && t <= et; t++){
				Geometry g = trajGeometries.get(t);
				if(g != null && !g.isEmpty()){
					double min_x = trajGeometries.get(t).getEnvelopeInternal().getMinX();
					double max_x = trajGeometries.get(t).getEnvelopeInternal().getMaxX();

					double min_y = trajGeometries.get(t).getEnvelopeInternal().getMinY();
					double max_y = trajGeometries.get(t).getEnvelopeInternal().getMaxY();

					//from all cells spanning through min_x to max_x
					for( Integer xCell = (int) (min_x/cell_x); xCell <= (int)(max_x/cell_x); xCell++ ){
						//from all cells spanning through min_y to max_y
						for( Integer yCell = (int) (min_y/cell_y); yCell <= (int) (max_y/cell_y); yCell++ ){
							
							Envelope e = new Envelope(min_x, max_x, min_y, max_y);
							if(new GeometryFactory().toGeometry(e).intersects(g)){
								String spacePartitionString = xCell.toString() + ";" + yCell.toString(); 
								String segmentId =  timePartitionString + spacePartitionString;
								
								if(trajSegments.containsKey(segmentId)){
									trajSegments.get(segmentId).addGeometry(t, g);
								} else{
									TSegment tSegment = new TSegment(traj.trajectoryID, segmentId);
									tSegment.addGeometry(t, g);
									trajSegments.put(segmentId, tSegment);
								}
								//st_partition_ids.add();
							}
						}
					}
				} else{
					if(g == null)
						System.out.println("Check " + t + "of\n" + traj);
				}
			}
			partitionStart = t++;
		}
		return trajSegments;
	}



}