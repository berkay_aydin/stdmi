package org.gsu.base;

/**
 * @author berkay
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

	private static String datasetDirectory;
	private static String queryDatasetDirectory;
	private static int d1;
	private static int d2;
	private static int x_partition;
	private static int y_partition;
	private static int time_step;
	private static int scanners;
	private static int numberOfQueries;
	private static int splits;

	public static Properties prop;
	private static boolean deleteOpt;

	public ConfigReader() {

		prop = new Properties();
		try {
			// System.out.println("Am i here");
			File configfile = new File("config.properties");
			FileInputStream fileInput = new FileInputStream(configfile);
			prop.load(fileInput);
			fileInput.close();
		} catch (FileNotFoundException e) {
			System.out.println("Exception: config.properties file not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Exception: config.properties cannot be read");
			e.printStackTrace();
		}

		datasetDirectory = prop.getProperty("datasetDirectory");
		queryDatasetDirectory = prop.getProperty("queryDirectory");
		String d1_str = prop.getProperty("d1", "100000");
		setD1(new Integer(d1_str.trim()));
		String d2_str = prop.getProperty("d2", "100000");
		setD2(new Integer(d2_str.trim()));
		String xp_str = prop.getProperty("x_partition", "4");
		setX_partition(new Integer(xp_str.trim()));
		String yp_str = prop.getProperty("y_partition", "4");
		setY_partition(new Integer(yp_str.trim()));
		String time_step_str = prop.getProperty("time_step", "50");
		setTime_step(new Integer(time_step_str.trim()));
		String splits_str = prop.getProperty("splits", "2");
		splits = new Integer(splits_str.trim());
		String scanners_str = prop.getProperty("scanners", "2");
		scanners = new Integer(scanners_str.trim());
		String delete_str = prop.getProperty("delete", "true");
		deleteOpt = new Boolean(delete_str.trim());
		String numberOfQueriesStr = prop.getProperty("numberOfQueries", "500");
		setNumberOfQueries(new Integer(numberOfQueriesStr.trim()));

	}

	public static String getDatasetDirectory() {
		return datasetDirectory;
	}

	public static String getQueryDatasetDirectory() {
		return queryDatasetDirectory;
	}

	public static int getD1() {
		return d1;
	}

	public static void setD1(int d1) {
		ConfigReader.d1 = d1;
	}

	public static int getD2() {
		return d2;
	}

	public static void setD2(int d2) {
		ConfigReader.d2 = d2;
	}

	public static int getX_partition() {
		return x_partition;
	}

	public static void setX_partition(int x_partition) {
		ConfigReader.x_partition = x_partition;
	}

	public static int getY_partition() {
		return y_partition;
	}

	public static void setY_partition(int y_partition) {
		ConfigReader.y_partition = y_partition;
	}

	public static int getTime_step() {
		return time_step;
	}

	public static void setTime_step(int time_step) {
		ConfigReader.time_step = time_step;
	}

	public static int getScanners() {
		return scanners;
	}

	public static boolean getDeleteOption() {
		return deleteOpt;
	}

	public static int getNumberOfQueries() {
		return numberOfQueries;
	}

	public static void setNumberOfQueries(int numberOfQueries) {
		ConfigReader.numberOfQueries = numberOfQueries;
	}

	public static int getSplits() {
		return splits;
	}
}
