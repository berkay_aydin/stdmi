package org.gsu.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.TreeMap;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

public class TSegment {

	public String trajId;
	public String segmentId;
	private TreeMap<Long, Geometry> geometryMap;
	private Envelope mbr;
	
	Long startTime;
	Long endTime;
	
	
	public TSegment(String trid, String sid){
		trajId = trid;
		segmentId = sid;
		geometryMap = new TreeMap<Long, Geometry>();
		mbr = new Envelope();
	}
	
	public void addGeometry(Long t, Geometry g){
		geometryMap.put(t, g);
		mbr.expandToInclude(g.getEnvelopeInternal());
		startTime = geometryMap.firstKey();
		endTime = geometryMap.lastKey();
	}
	
	public byte[] serializeGeometries(){
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	        ObjectOutputStream os = new ObjectOutputStream(out);
	        os.writeObject(geometryMap);
	        return out.toByteArray();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static TreeMap<Long, Geometry> deserializeGeometries(byte[] data){
		
		try {
			ByteArrayInputStream in = new ByteArrayInputStream(data);
			ObjectInputStream is = new ObjectInputStream(in);
			return (TreeMap<Long, Geometry>) is.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
}
