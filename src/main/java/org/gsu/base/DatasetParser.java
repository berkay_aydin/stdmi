package org.gsu.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

/**
 * @author vijay.akkineni; berkay
 */
public class DatasetParser {

	private String dataDir;
	private ArrayList<File> featureFiles;

	public Map<Integer, Trajectory> trajectoryMap;

	public DatasetParser() {

		dataDir = ConfigReader.getDatasetDirectory();
		featureFiles = new ArrayList<File>();
		trajectoryMap = new HashMap<Integer, Trajectory>();

		File datasetFolder = new File(dataDir);
		findFeatureFiles(datasetFolder);

		System.out.println("Files: " + listFeatureFiles());
		for (int i = 0; i < featureFiles.size(); i++) {
			readFeatureFile(featureFiles.get(i));
		}

	}

	public void findFeatureFiles(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				findFeatureFiles(fileEntry);
			} else {
				System.out.println(fileEntry.getName());
				if (!fileEntry.getName().contains("metadata") && !fileEntry.getName().contains(".DS_Store")) {
					featureFiles.add(fileEntry);
				}
			}
		}
	}

	public String listFeatureFiles() {
		String s = "";
		for (int i = 0; i < featureFiles.size(); i++) {
			s += featureFiles.get(i) + ", ";
		}
		return s;
	}

	public int readFeatureFile(File file) {
		BufferedReader br;
		int count = 0;
		try {
			String line;
			br = new BufferedReader(new FileReader(file));

			while ((line = br.readLine()) != null) {

				String[] result = line.split("\\t", 3); // result[0]: instance
														// id
				// result[1]: timestamp
				// result[2]: geom
				Integer trajectoryID = new Integer(result[0]);
				Integer timestamp = new Integer(result[1]);
				WKTReader geomReader = new WKTReader();
				Geometry geom = null;
				try {
					geom = geomReader.read(result[2]);
				} catch (ParseException e) {
					System.out.println("Exception: geometry cannot be read (" + trajectoryID + "," + timestamp + ")");
					e.printStackTrace();
				}

				Trajectory trajectory = null;
				if ((trajectory = trajectoryMap.get(trajectoryID)) == null) { // then
																				// add
																				// a
																				// new
																				// instance
					count++;
					trajectory = new Trajectory(trajectoryID, null, null);
					trajectory.addGeometry(timestamp, geom);

					trajectoryMap.put(trajectoryID, trajectory);

				} else { // add a new geometry
					trajectoryMap.get(trajectoryID).addGeometry(timestamp, geom);
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			System.out.println("Exception: feature file cannot be found (" + file + ")");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Exception: feature file cannot be read (" + file + ")");
			e.printStackTrace();
		}
		return count;
	}
}
