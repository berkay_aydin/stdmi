package org.gsu.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

public class Trajectory {
    private final TreeMap<Long, Geometry> geometryMap;
    private final Envelope mbr;
    String trajectoryID;
    Long startTime;
    Long endTime;

    public Trajectory(long id, ArrayList<Long> timestamps, ArrayList<Geometry> polygons) {
        this.trajectoryID = Long.toString(id);
        this.geometryMap = new TreeMap<Long, Geometry>();
        mbr = new Envelope();
        if (timestamps != null && polygons != null) {
            if (timestamps.size() == geometryMap.size()) {
                for (int i = 0; i < timestamps.size(); i++) {
                    mbr.expandToInclude(polygons.get(i).getEnvelopeInternal());
                    geometryMap.put(timestamps.get(i), polygons.get(i));
                }
            }
        }
        if (geometryMap.size() != 0) {
            startTime = geometryMap.firstKey();
            endTime = geometryMap.lastKey();
        }
    }

    public Trajectory(String id, byte[] data) {
        this.trajectoryID = id;
        this.geometryMap = deserializeGeometries(data);
        mbr = new Envelope(); //there is no need but let's do it
        validateMbr();
        validateStartEndTimes();
    }

    public byte[] serializeGeometries() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(geometryMap);
            return out.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static TreeMap<Long, Geometry> deserializeGeometries(byte[] data) {

        try {
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            ObjectInputStream is = new ObjectInputStream(in);
            return (TreeMap<Long, Geometry>) is.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void addGeometry(long timestamp, Geometry geom) {
        if (geometryMap != null) {
            mbr.expandToInclude(geom.getEnvelopeInternal());
            this.geometryMap.put(timestamp, geom);
            startTime = geometryMap.firstKey();
            endTime = geometryMap.lastKey();
        }
    }

    public TreeSet<String> partition() {
        STPartitioner stPartitioner = new STPartitioner();
        return stPartitioner.getPartitions(this);
    }

    public String toString() {
        String s = "ID: " + trajectoryID + "\n";
        if (isValid()) {
            if (!isEmpty()) {

                for (long key : geometryMap.keySet()) {
                    s += "\t" + "Time: " + key + "\t\tGeom: " + geometryMap.get(key) + "\n";
                }
                s += "<<";
            } else {
                s += "\tEmpty trajectory\n<<";
            }

        } else {
            s += "\tIs not a valid trajectory \n<<";
        }
        return s;
    }


    private boolean isEmpty() {
        if (isValid()) {
            return geometryMap.size() == 0;
        } else {
            System.out.println("Trajectory is not valid!");
            return false;
        }

    }


    public boolean isValid() {
        if (geometryMap != null) {
            validateStartEndTimes();
            return true;
        }
        System.out.println("Trajectory is not valid!");
        return false;
    }


    public void validateStartEndTimes() {
        if (geometryMap != null) {
            if (geometryMap.size() != 0) {
                startTime = geometryMap.firstKey();
                endTime = geometryMap.lastKey();
            }
        }
    }

    public void validateMbr() {
        for (Entry<Long, Geometry> geoEntry : geometryMap.entrySet()) {
            mbr.expandToInclude(geoEntry.getValue().getEnvelopeInternal());
        }
    }

    public String getTrajectoryId() {
        return trajectoryID;
    }
    public Long getStartTime() {
        return startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public TreeMap<Long, Geometry> getGeometryMap() {
        return geometryMap;
    }

    public Envelope getMbr() {
        return mbr;
    }

    public boolean intersectsAt(Geometry polygon, Long time) {
        Geometry g = geometryMap.get(time);
        if (g != null) {
            return g.intersects(polygon);
        } else {
            return false;
        }

    }

}
