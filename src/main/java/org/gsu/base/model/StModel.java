package org.gsu.base.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;
import org.gsu.base.STPartitioner;
import org.gsu.base.Trajectory;
import org.gsu.base.model.index.InvertedIndex;
import org.gsu.db.DbOperationsManager;
import org.gsu.stats.RunStatistics;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.io.WKBWriter;

public class StModel {

    final WKBWriter wkbWriter = new WKBWriter();
    private final String trajectoryTableName;
    // private final String metaTableName;
    private final DbOperationsManager dbOperationsManager;
    final STPartitioner stPartitioner = new STPartitioner();
    private InvertedIndex index = null;
    private boolean isIndexed = false;

    public void createTables() {
        dbOperationsManager.createTable(trajectoryTableName);
    }

    public StModel(Connector connector, String trajectoryTableName, String metaTableName, boolean isIndexActive) {
        this.trajectoryTableName = trajectoryTableName;
        // this.metaTableName = metaTableName;
        dbOperationsManager = new DbOperationsManager(connector);

        if (isIndexActive) {
            isIndexed = true;
            index = new InvertedIndex();
        }
    }

    public void populateDatabase(Map<Integer, Trajectory> trajectoryMap) {

        BatchWriter bw = null;
        try {
            bw = dbOperationsManager.getBatchWriter(trajectoryTableName);

            for (Map.Entry<Integer, Trajectory> trajEntry : trajectoryMap.entrySet()) {
                Integer trajID = trajEntry.getKey();
                Trajectory trajectory = trajEntry.getValue();

                if (isIndexed) {
                    index.insert(trajectory);
                }

                TreeSet<String> partitions = stPartitioner.getPartitions(trajectory);
                byte[] serializedGeometries = trajectory.serializeGeometries();

                for (String p : partitions) {
                    Mutation m = createMutation(p, "trajId", trajID.toString(), serializedGeometries);
                    dbOperationsManager.insertMutation(bw, m);
                }

                // insert metadata (for now I am not sure if this is necessary
                // at all)
                /*
                 * Geometry mbrGeom = factory.toGeometry(trajectory.getMbr());
				 * String interval = trajectory.getStartTime() + ":" +
				 * trajectory.getEndTime();
				 * metaMutations.add(createMutation(trajID.toString(), "MBR",
				 * interval, wkbWriter.write(mbrGeom)));
				 * dbOperationsManager.insertMutations(metaTableName,
				 * metaMutations);
				 */
                // flush

            }

            bw.flush();

        } catch (AccumuloException | AccumuloSecurityException | TableNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
            } catch (MutationsRejectedException e) {
                e.printStackTrace();
            }
        }

        if (isIndexed) {
            System.out.println("printing index size");
            RunStatistics.calcSizeOfIndex(index);
            System.out.println("printing index size done");
        }

    }

    public boolean search(Trajectory queryTrajectory, String spatialPredicate, String temporalPredicate, Long startTime, Long endTime) {

        boolean isSearchSuccess = false;

        GeometryFactory factory = new GeometryFactory();
        Geometry queryGeom = factory.toGeometry(queryTrajectory.getMbr());

        TreeSet<String> partitions = queryTrajectory.partition();

        if (partitions == null || partitions.size() == 0) {
            return isSearchSuccess;
        }

        // Set<String> ids = searchMeta(queryGeom, startTime, endTime,
        // spatialPredicate, temporalPredicate);
        BatchScanner scanner = null;
        try {
            scanner = dbOperationsManager.createScanner(trajectoryTableName);

            List<Range> ranges = new ArrayList<Range>();
            for (String id : partitions) {
                ranges.add(new Range(id));
            } // here we search for the partitions not for trajectory ids
            scanner.setRanges(ranges);

            for (Map.Entry<Key, Value> entry : scanner) {
                Text row = entry.getKey().getRow();
                byte[] value = entry.getValue().get();

                TreeMap<Long, Geometry> geometries = Trajectory.deserializeGeometries(value);

                NavigableMap<Long, Geometry> subGeom = geometries.subMap(startTime, true, endTime, true);

                if (subGeom == null) {
                    continue;
                }
                if (subGeom.size() == 0) {
                    continue;
                }

                for (Entry<Long, Geometry> subEntry : subGeom.entrySet()) {
                    Geometry geometry = subEntry.getValue();
                    if (geometry != null) {
                        if (geometry.intersects(queryGeom)) {
                            // System.out.println("*");
                            System.out.println("* " + row.toString() + " : " + entry.getKey().getColumnQualifier() + " : " + geometry);
                            isSearchSuccess = true;
                        }
                    }
                    // System.out.println(row.toString() + " : " +
                    // entry.getKey().getColumnQualifier() + " : " + geometry);
                }
            }
        } catch (TableNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return isSearchSuccess;
    }

    public boolean nonSTSearch(String trajID) {
        boolean isSearchSuccess = false;

        Set<String> partitions = new HashSet<>();
        List<Range> ranges = new ArrayList<Range>();
        if (isIndexed) {
            partitions = new HashSet<>(index.searchTrajectoryId(trajID));

            if (partitions.isEmpty()) {
                System.out.println("Partitions Empty Set~!!!!");
                return isSearchSuccess;
            }

            for (String id : partitions) {
                ranges.add(new Range(id));
            } // here we search for the partitions not for trajectory ids
        } else {
            ranges.add(new Range());
        }
        BatchScanner scanner = null;
        try {

            System.out.println("Ranges : " + ranges);
            scanner = dbOperationsManager.createScanner(trajectoryTableName);

            scanner.setRanges(ranges);

            for (Map.Entry<Key, Value> entry : scanner) {
                // Text row = entry.getKey().getRow();
                String colq = entry.getKey().getColumnQualifier().toString();
                byte[] value = entry.getValue().get();

//                System.out.println("colq = " + colq);

                TreeMap<Long, Geometry> geometries = Trajectory.deserializeGeometries(value);

                if (colq.equals(trajID)) {
                    System.out.println("Result Trajectory ID: " + trajID + geometries);
                    isSearchSuccess = true;
                }
            }
        } catch (TableNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return isSearchSuccess;
    }

    private Mutation createMutation(String rowKey, String columnFamily, String columnQualifier, byte[] value) {
        Mutation mutation = new Mutation(rowKey);
        mutation.put(columnFamily.getBytes(), columnQualifier.getBytes(), value);
        RunStatistics.dbMemory += mutation.estimatedMemoryUsed();
        return mutation;

    }

}
