package org.gsu.base.model.index;

import org.gsu.base.ConfigReader;
import org.gsu.base.Trajectory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.index.strtree.SIRtree;
import org.gsu.stats.RunStatistics;

public class GRtIndex {

    Map<Coordinate, SIRtree> grid;
    public static int deltaX;
    public static int deltaY;

    public GRtIndex() {

        deltaX = ConfigReader.getX_partition();
        deltaY = ConfigReader.getY_partition();
        grid = new HashMap<Coordinate, SIRtree>();

    }

    public void insert(Trajectory tr) {

        long istartTime = System.currentTimeMillis();
        Envelope env = tr.getMbr();
        long startTime = tr.getStartTime();
        long endTime = tr.getEndTime();

        HashSet<Coordinate> cells = getCells(env);
        if (cells.size() == 0) {
            System.out.println("There is a problem when getting cells (GRt Index insertion)");
        } else {
            for (Coordinate c : cells) {
                if (grid.containsKey(c)) {
                    grid.get(c).insert(startTime, endTime, tr.getTrajectoryId());
                } else {
                    SIRtree cellIntervalRtree = new SIRtree();
                    cellIntervalRtree.insert(startTime, endTime, tr.getTrajectoryId());
                    grid.put(c, cellIntervalRtree);
                }
            }
        }

        long iendTime = System.currentTimeMillis();
        RunStatistics.indexInsertionTime += iendTime - istartTime;
    }

    public ArrayList<String> search(Trajectory tr, String temporalPredicate, String spatialPredicate) {
        ArrayList<String> results = new ArrayList<String>();

        long startTime = tr.getStartTime();
        long endTime = tr.getEndTime();

        if (spatialPredicate.equalsIgnoreCase("none")) {
            if (isSupportedTemporalOperation(temporalPredicate)) {
                results.addAll(temporalSearch(temporalPredicate, grid.keySet(), startTime, endTime));
            } else {
                System.out.println("This temporal predicate is not yet implemented");
            }

        } else if (isSupportedSpatialOperation(spatialPredicate)) {
            HashSet<Coordinate> cells = spatialSearch(spatialPredicate, tr.getMbr());
            System.out.println("Spatial Operation supported: " + tr.getMbr().toString());
            System.out.println(cells);
            for (Coordinate c : cells) {
                SIRtree tree = grid.get(c);
                if (tree != null) {
                    List<String> l = tree.query(startTime, endTime);
                    results.addAll(l);
                }
            }
            if (isSupportedTemporalOperation(temporalPredicate)) {
                results.addAll(temporalSearch(temporalPredicate, grid.keySet(), startTime, endTime));
            } else {
                System.out.println("This temporal predicate is not yet implemented");
            }
        } else {
            System.out.println("This spatial predicate is not yet implemented");
        }
        return results;
    }

    private static boolean isSupportedSpatialOperation(String predicate) {
        if (predicate.equalsIgnoreCase("overlaps")) {
            return true;
        } else if (predicate.equalsIgnoreCase("disjoint")) {
            return true;
        } else if (predicate.equalsIgnoreCase("touches")) {
            return true;
        } else if (predicate.equalsIgnoreCase("contains")) {
            return true;
        } else if (predicate.equalsIgnoreCase("covers")) {
            return true;
        } else if (predicate.equalsIgnoreCase("crosses")) {
            return true;
        } else if (predicate.equalsIgnoreCase("within")) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isSupportedTemporalOperation(String predicate) {
        if (predicate.equalsIgnoreCase("overlaps")) {
            return true;
        } else if (predicate.equalsIgnoreCase("startsBefore")) {
            return true;
        } else if (predicate.equalsIgnoreCase("startsAfter")) {
            return false;
        } else if (predicate.equalsIgnoreCase("endsAfter")) {
            return true;
        } else if (predicate.equalsIgnoreCase("endsBefore")) {
            return false;
        } else if (predicate.equalsIgnoreCase("meets")) {
            return false;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private ArrayList<String> temporalSearch(String predicate, Set<Coordinate> cells, long startTime, long endTime) {
        ArrayList<String> resultingTrajIds = new ArrayList<String>();
        if (predicate.equalsIgnoreCase("none")) {
            for (Coordinate c : cells) {
                List<String> l = grid.get(c).query(0.0, Double.MAX_VALUE);
                resultingTrajIds.addAll(l);
            }
        } else if (predicate.equalsIgnoreCase("overlaps")) {
            for (Coordinate c : cells) {
                List<String> l = grid.get(c).query(startTime, endTime);
                resultingTrajIds.addAll(l);
            }
        } else if (predicate.equalsIgnoreCase("startsBefore")) {
            for (Coordinate c : cells) {
                List<String> l = grid.get(c).query(0.0, startTime);
                resultingTrajIds.addAll(l);
            }
        } else if (predicate.equalsIgnoreCase("startsAfter")) {
            return resultingTrajIds;
        } else if (predicate.equalsIgnoreCase("endsAfter")) {
            for (Coordinate c : cells) {
                List<String> l = grid.get(c).query(endTime, Double.MAX_VALUE);
                resultingTrajIds.addAll(l);
            }
        } else if (predicate.equalsIgnoreCase("endsBefore")) {
            return resultingTrajIds;
        } else if (predicate.equalsIgnoreCase("meets")) {
            return resultingTrajIds;
        } else {
            return resultingTrajIds;
        }
        return resultingTrajIds;
    }

    private HashSet<Coordinate> spatialSearch(String predicate, Envelope mbr) {
        if (predicate.equalsIgnoreCase("overlaps")) {
            return getCells(mbr);
        } else if (predicate.equalsIgnoreCase("disjoint")) {
            return new HashSet<Coordinate>(grid.keySet());
        } else if (predicate.equalsIgnoreCase("touches")) {
            return getCells(mbr);
        } else if (predicate.equalsIgnoreCase("contains")) {
            return getCells(mbr);
        } else if (predicate.equalsIgnoreCase("covers")) {
            return getCells(mbr);
        } else if (predicate.equalsIgnoreCase("crosses")) {
            return getCells(mbr);
        } else if (predicate.equalsIgnoreCase("within")) {
            return getCells(mbr);
        } else {
            System.out.println("This predicate is not implemented: Spatial search");
            return new HashSet<Coordinate>();
        }

    }

    private static HashSet<Coordinate> getCells(Envelope mbr) {
        HashSet<Coordinate> mbrCells = new HashSet<Coordinate>();

        double min_x = mbr.getMinX();
        double max_x = mbr.getMaxX();
        double min_y = mbr.getMinY();
        double max_y = mbr.getMaxY();

        //from all cells spanning through min_x to max_x
        for (Integer xCell = (int) (min_x / deltaX); xCell <= (int) (max_x / deltaX); xCell++) {
            //from all cells spanning through min_y to max_y
            for (Integer yCell = (int) (min_y / deltaY); yCell <= (int) (max_y / deltaY); yCell++) {
                Coordinate cell = new Coordinate(xCell.doubleValue(), yCell.doubleValue());
                mbrCells.add(cell);
            }
        }
        return mbrCells;
    }

}
