package org.gsu.base.model.index;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.gsu.base.Trajectory;
import org.gsu.stats.RunStatistics;


public class InvertedIndex {

    Map<String, ArrayList<String>> invIndex;
    public static int deltaX;
    public static int deltaY;

    public InvertedIndex() {
        invIndex = new TreeMap<String, ArrayList<String>>();
    }

    public void insert(Trajectory tr) {
        long iStartTime = System.currentTimeMillis();
        TreeSet<String> partitions = tr.partition();
        if (partitions == null || partitions.size() == 0) {
            System.out.println("There is a problem here");
        }
        for (String partition : partitions) {
            if (invIndex.containsKey(tr.getTrajectoryId())) {
                invIndex.get(tr.getTrajectoryId()).add(partition);
            } else {
                invIndex.put(tr.getTrajectoryId(), new ArrayList<String>());
                invIndex.get(tr.getTrajectoryId()).add(partition);
            }
        }

        long iEndTime = System.currentTimeMillis();
        RunStatistics.indexInsertionTime += iEndTime - iStartTime;
    }

    public HashSet<String> searchTrajectoryId(String trajId) {
        if (invIndex.get(trajId) != null) {
            return new HashSet<String>(invIndex.get(trajId));
        } else {
            return new HashSet<String>();
        }
    }

}
