package org.gsu.base.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;
import org.gsu.base.Trajectory;
import org.gsu.base.model.index.GRtIndex;
import org.gsu.db.DbOperationsManager;
import org.gsu.stats.RunStatistics;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;
import com.vividsolutions.jts.io.WKBWriter;

/**
 * Created by vijay.akkineni on 12/13/14.
 */
public class ClassicalModel1 {

	private final String trajectoryTableName;
	private final String metaTableName;
	private final DbOperationsManager dbOperationsManager;
	final WKBWriter wkbWriter = new WKBWriter();
	private GRtIndex index = null;
	private boolean isIndexed = false;

	public ClassicalModel1(Connector connector, String trajectoryTableName, String metaTableName, boolean isIndexActive) {
		this.trajectoryTableName = trajectoryTableName;
		this.metaTableName = metaTableName;
		dbOperationsManager = new DbOperationsManager(connector);

		if (isIndexActive) {
			isIndexed = true;
			index = new GRtIndex();
		}
	}

	public void createTables() {
		dbOperationsManager.createTable(trajectoryTableName);
		dbOperationsManager.createTable(metaTableName);
	}

	public void populateDatabase(Map<Integer, Trajectory> trajectoryMap) {

		BatchWriter metaBatchWriter = null;
		BatchWriter trajBatchWriter = null;
		GeometryFactory factory = new GeometryFactory();
		try {
			metaBatchWriter = dbOperationsManager.getBatchWriter(metaTableName);
			trajBatchWriter = dbOperationsManager.getBatchWriter(trajectoryTableName);
			for (Map.Entry<Integer, Trajectory> trajEntry : trajectoryMap.entrySet()) {
				Integer trajID = trajEntry.getKey();

				Trajectory trajectory = trajEntry.getValue();

				if (isIndexed) {
					index.insert(trajectory);
				}

				for (Map.Entry<Long, Geometry> trajMapEntry : trajectory.getGeometryMap().entrySet()) {
					Long timestamp = trajMapEntry.getKey();
					Geometry geom = trajMapEntry.getValue();
					byte[] geomBytes = wkbWriter.write(geom);
					dbOperationsManager.insertMutation(trajBatchWriter,
							createMutation(Integer.toString(trajID), "st", timestamp.toString(), geomBytes));
				}

				Geometry mbrGeom = factory.toGeometry(trajectory.getMbr());
				String interval = trajectory.getStartTime() + ":" + trajectory.getEndTime();
				dbOperationsManager.insertMutation(metaBatchWriter, createMutation(trajID.toString(), "MBR", interval, wkbWriter.write(mbrGeom)));

			}
			metaBatchWriter.flush();
			trajBatchWriter.flush();
		} catch (AccumuloException | AccumuloSecurityException | TableNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (metaBatchWriter != null) {
				try {
					metaBatchWriter.close();
				} catch (MutationsRejectedException e) {
					e.printStackTrace();
				}
			}

			if (trajBatchWriter != null) {
				try {
					trajBatchWriter.close();
				} catch (MutationsRejectedException e) {
					e.printStackTrace();
				}
			}
		}

		if (isIndexed) {
			RunStatistics.calcSizeOfIndex(index);
		}
	}

	private Mutation createMutation(String rowKey, String columnFamily, String columnQualifier, byte[] value) {
		Mutation mutation = new Mutation(rowKey);
		mutation.put(columnFamily.getBytes(), columnQualifier.getBytes(), value);
		RunStatistics.dbMemory += mutation.estimatedMemoryUsed();
		return mutation;
	}

	public boolean search(Trajectory queryTrajectory, String spatialPredicate, String temporalPredicate, long startTime, long endTime) {

		boolean isSearchSuccess = false;

		GeometryFactory factory = new GeometryFactory();
		Geometry queryGeom = factory.toGeometry(queryTrajectory.getMbr());

		// Get this from the index if it is indexed

		Set<String> ids = new HashSet<>();
		if (isIndexed) {
			ids = new HashSet<>(index.search(queryTrajectory, temporalPredicate, spatialPredicate));
			System.out.println("ID's from the index: " + ids);
		} else {
			ids = searchMeta(queryGeom, startTime, endTime, spatialPredicate, temporalPredicate);
			System.out.println("ID's from the meta table: " + ids);
		}

		BatchScanner scanner = null;
		try {
			scanner = dbOperationsManager.createScanner(trajectoryTableName);

			if (ids.isEmpty()) {
				// System.out.println("No ID's found from the index");
				return isSearchSuccess;
			}

			List<Range> ranges = new ArrayList<Range>();
			for (String id : ids) {
				ranges.add(new Range(id));
			}
			scanner.setRanges(ranges);

			for (Map.Entry<Key, Value> entry : scanner) {
				// Text row = entry.getKey().getRow();
				Long time = new Long(entry.getKey().getColumnQualifier().toString());
				byte[] value = entry.getValue().get();
				WKBReader reader = new WKBReader();
				try {
					Geometry geometry = reader.read(value);
					if (queryTrajectory.intersectsAt(geometry, time)) {
						// System.out.println("*" + row.toString() + " : " +
						// geometry);
						isSearchSuccess = true;
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

		} catch (TableNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}

		return isSearchSuccess;
	}

	public Set<String> searchMeta(Geometry queryGeom, long startTime, long endTime, String spatialPredicate, String temporalPredicate) {

		Set<String> trajectoryIDs = new HashSet<>();
		BatchScanner scanner = null;
		try {
			scanner = dbOperationsManager.createMetaScanner(metaTableName, queryGeom, startTime, endTime, spatialPredicate, temporalPredicate);

			List<Range> ranges = new ArrayList<Range>();
			ranges.add(new Range());
			scanner.setRanges(ranges);

			for (Map.Entry<Key, Value> entry : scanner) {
				Text row = entry.getKey().getRow();
				trajectoryIDs.add(row.toString());
			}

		} catch (TableNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return trajectoryIDs;
	}

}
