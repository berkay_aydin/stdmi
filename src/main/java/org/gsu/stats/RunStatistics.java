package org.gsu.stats;

import net.sourceforge.sizeof.SizeOf;

import org.gsu.base.model.index.GRtIndex;
import org.gsu.base.model.index.InvertedIndex;

import com.javamex.classmexer.MemoryUtil;

/**
 * Run Statistics Created by vijay.akkineni on 12/23/14.
 */
public class RunStatistics {

	public static long indexInsertionTime = 0l;
	public static long indexSize = 0l;
	public static long dbInsertionTime = 0l;
	public static long dbMemory = 0l;
	public static long querySearchTime = 0l;
	public static int hits = 0;
	public static int misses = 0;

	public static void printIndexInsertionTime() {
		System.out.println("Index Insertion Time: " + indexInsertionTime);
	}

	public static void printDBInsertionTime() {
		System.out.println("DB Insertion Time: " + dbInsertionTime);
	}

	public static void printDBMemory() {
		System.out.println("DB Memory consumed: " + dbMemory);
	}

	public static void calculateIndexSize(GRtIndex index) {
		indexSize = MemoryUtil.deepMemoryUsageOf(index);
	}

	public static void printIndexSize() {
		System.out.println("Index size: " + indexSize);
	}

	public static void calculateIndexSize(InvertedIndex index) {
		indexSize = MemoryUtil.deepMemoryUsageOf(index);
	}

	public static void calcSizeOfIndex(GRtIndex index) {
		SizeOf.skipStaticField(true); // java.sizeOf will not compute static
										// fields
		SizeOf.skipFinalField(true);
		SizeOf.skipFlyweightObject(true);
		SizeOf.turnOffDebug();
		indexSize = SizeOf.deepSizeOf(index);
	}

	public static void calcSizeOfIndex(InvertedIndex index) {
		SizeOf.skipStaticField(true); // java.sizeOf will not compute static
										// fields
		SizeOf.skipFinalField(true);
		SizeOf.skipFlyweightObject(true);
		SizeOf.turnOffDebug();
		indexSize = SizeOf.deepSizeOf(index);
	}

	public static void printQuerySearchTime() {
		System.out.println("Query Search Time: " + querySearchTime);
	}

	public static void printHits() {
		System.out.println("Number of Hits: " + hits);
	}

	public static void printMisses() {
		System.out.println("Number of Misses: " + misses);
	}

}
