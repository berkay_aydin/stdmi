package org.gsu.db;

import com.beust.jcommander.Parameter;
import org.apache.accumulo.core.cli.ClientOnRequiredTable;

/**
 * Created by vijay.akkineni on 12/15/14.
 */
public class StdmClientOnRequiredTable extends ClientOnRequiredTable {

    @Parameter(names = {"-m", "--model"}, required = true, description = "model to use")
    public String model = null;

    @Parameter(names = {"-sc", "--skipcreation"}, required = true, description = "skip data creation")
    public String skipC = "false";

    @Parameter(names = {"-ui", "--useindexing"}, required = true, description = "use indexing")
    public String useIndex = "false";

    @Parameter(names = {"-qt", "--queryType"}, required = true, description = "Query Type")
    public String queryType = "timestamp";
}
