package org.gsu.db;

import java.util.Collection;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.TableExistsException;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.security.Authorizations;
import org.gsu.base.Trajectory;
import org.gsu.db.filters.Cdm1Filter;
import org.gsu.stats.RunStatistics;

import com.vividsolutions.jts.geom.Geometry;

public class DbOperationsManager {

	public Connector getConnector() {
		return connector;
	}

	private final Connector connector;

	public DbOperationsManager(Connector connector) {
		this.connector = connector;
	}

	public void insert(Trajectory t, String tableName, String method) {

	}

	public void search() {

	}

	public void createTable(String tableName) {
		try {
			if (!connector.tableOperations().exists(tableName)) {
				connector.tableOperations().create(tableName);
				connector.tableOperations().setProperty(tableName, "table.cache.block.enable", "true");
				connector.tableOperations().setProperty(tableName, "table.cache.index.enable", "true");
				connector.tableOperations().setProperty(tableName, "table.bloom.enabled", "true");
			} else {
				connector.tableOperations().delete(tableName);
				connector.tableOperations().create(tableName);
				connector.tableOperations().setProperty(tableName, "table.cache.block.enable", "true");
				connector.tableOperations().setProperty(tableName, "table.cache.index.enable", "true");
				connector.tableOperations().setProperty(tableName, "table.bloom.enabled", "true");
			}
		} catch (AccumuloException | AccumuloSecurityException | TableExistsException | TableNotFoundException e) {
			e.printStackTrace();
		}
	}

	public BatchScanner createScanner(String tableName) throws TableNotFoundException {
		Authorizations auths = new Authorizations();
		BatchScanner scanner = connector.createBatchScanner(tableName, auths, 1);
		return scanner;
	}

	public BatchScanner createMetaScanner(String tableName, Geometry queryGeom, long startTime, long endTime, String spatialPredicate,
			String temporalPredicate) throws TableNotFoundException {
		Authorizations auths = new Authorizations();
		BatchScanner scanner = connector.createBatchScanner(tableName, auths, 1);

		IteratorSetting stFilterSettings = new IteratorSetting(100, "Cdm1Filter", Cdm1Filter.class);

		stFilterSettings.addOption("queryGeom", queryGeom.toString());
		stFilterSettings.addOption("startTime", Long.toString(startTime));
		stFilterSettings.addOption("endTime", Long.toString(endTime));
		stFilterSettings.addOption("temporalPredicate", temporalPredicate);
		stFilterSettings.addOption("spatialPredicate", spatialPredicate);

		scanner.addScanIterator(stFilterSettings);
		return scanner;
	}

	public void insertMutations(String tableName, Collection<Mutation> mutations) {

		long startTime = System.currentTimeMillis();
		try {
			BatchWriter bw = getBatchWriter(tableName);
			bw.addMutations(mutations);
		} catch (AccumuloException | AccumuloSecurityException | TableNotFoundException e) {
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis();

		RunStatistics.dbInsertionTime += endTime - startTime;

	}

	public void insertMutation(BatchWriter bw, Mutation mutation) {

		long startTime = System.currentTimeMillis();

		try {
			bw.addMutation(mutation);
		} catch (MutationsRejectedException e) {
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis();

		RunStatistics.dbInsertionTime += endTime - startTime;
	}

	public BatchWriter getBatchWriter(String tableName) throws AccumuloException, AccumuloSecurityException, TableNotFoundException {
		BatchWriterConfig config = new BatchWriterConfig();
		return connector.createBatchWriter(tableName, config);
	}

}

// public static void main(String[] args) throws AccumuloSecurityException,
// AccumuloException, TableNotFoundException {
//
//
// BatchWriter bw = mtbw.getBatchWriter(tableName);
// GeometryFactory factory = new GeometryFactory();
//
// Double x1 = new Double(1.0);
// Double y1 = new Double(3.0);
// Double x2 = new Double(2.0);
// Double y2 = new Double(4.0);
// Envelope envelopeIn = new Envelope(x1, x2, y1, y2);
// WKTWriter writer = new WKTWriter();
//
// for (int i = 0; i < 100; i++) {
//
// Mutation m = new Mutation(new Text(String.format("%s", i)));
// byte[] columnFamily = "MBR".getBytes();
// byte[] columnQual = "meta".getBytes();
// String wkt = writer.write(factory.toGeometry(envelopeIn));
// m.put(columnFamily, columnQual, wkt.getBytes());
//
// bw.addMutation(m);
// }
//
// Envelope envelopeIn2 = new Envelope(1.0, 1.0, 1.0, 1.0);
//
// for (int i = 100; i < 200; i++) {
//
// Mutation m = new Mutation(new Text(String.format("%s", i)));
// byte[] columnFamily = "MBR".getBytes();
// byte[] columnQual = "meta".getBytes();
// String wkt = writer.write(factory.toGeometry(envelopeIn2));
// m.put(columnFamily, columnQual, wkt.getBytes());
//
// bw.addMutation(m);
// }
//
// if (bw != null) {
// mtbw.close();
// }
//
// BatchScanner scan =
// connector.createBatchScanner(tableName, auths, 1);
// IteratorSetting mbrFilterSettings = new IteratorSetting(
// 30,
// "mbrFilter",
// SpatialFilter.class);
//
// mbrFilterSettings.addOption("envelope", "1.0,2.0,3.0,4.0");
//
// SpatialFilter.setEnvelope(mbrFilterSettings);
// scan.addScanIterator(mbrFilterSettings);
//
// List<Range> ranges = new ArrayList<Range>();
// ranges.add(new Range(new Text("150"), new Text("50")),"MBR");
// // first fetch the instance specified by instId
// scan.setRanges(ranges);
//
// for (Map.Entry<Key, Value> entry : scan) {
// Text row = entry.getKey().getRow();
// Value value = entry.getValue();
// System.out.println(row.toString() + " : " + value.toString());
// }
//
// }