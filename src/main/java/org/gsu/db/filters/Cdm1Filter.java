package org.gsu.db.filters;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.Filter;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;

import java.io.IOException;
import java.util.Map;

/**
 * Created by vijay.akkineni on 12/13/14.
 */
public class Cdm1Filter extends Filter {

    private Geometry queryGeom;
    private long queryStartTime;
    private long queryEndTime;
    private String temporalPredicate;
    private String spatialPredicate;

    @Override
    public boolean accept(Key key, Value value) {
        if (key.getColumnFamily().toString().equalsIgnoreCase("MBR")) {
            String[] intervals = key.getColumnQualifier().toString().trim().split(":");
            long startTime = new Long(intervals[0].trim());
            long endTime = new Long(intervals[1].trim());

            if (checkTemporalPredicate(temporalPredicate, startTime, endTime, queryStartTime, queryEndTime)) {

                WKBReader reader = new WKBReader();
                Geometry geom = null;
                try {
                    geom = reader.read(value.get());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (checkSpatialPredicate(spatialPredicate, queryGeom, geom)) {
                    return true;
                }

            }
        }
        return false;
    }

    private boolean checkTemporalPredicate(String temporalPredicate, long startTime, long endTime, long queryStartTime, long queryEndTime) {

        if (temporalPredicate.equalsIgnoreCase("none")) {
            return true;
        } else if (temporalPredicate.equalsIgnoreCase("overlaps")) {
            return (queryStartTime <= endTime && queryEndTime >= startTime);
        } else {
            return false;
        }
    }

    private boolean checkSpatialPredicate(String spatialPredicate, Geometry queryGeom, Geometry geom) {
        if (spatialPredicate.equalsIgnoreCase("none")) {
            return true;
        } else if (spatialPredicate.equalsIgnoreCase("overlaps")) {
            return geom.getEnvelopeInternal().intersects(queryGeom.getEnvelopeInternal());
        } else {
            return false;
        }
    }

    @Override
    public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env) throws IOException {
        super.init(source, options, env);

        WKTReader reader = new WKTReader();
        String queryGeomWkt = options.get("queryGeom");

        try {
            queryGeom = reader.read(queryGeomWkt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        queryStartTime = new Long(options.get("startTime"));
        queryEndTime = new Long(options.get("endTime"));
        temporalPredicate = options.get("temporalPredicate");
        spatialPredicate = options.get("spatialPredicate");
    }

}
