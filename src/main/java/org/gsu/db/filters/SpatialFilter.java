package org.gsu.db.filters;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.Filter;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.user.RegExFilter;

import java.io.IOException;
import java.util.Map;

/**
 * Created by vijay.akkineni on 12/13/14.
 */
public class SpatialFilter extends Filter {

    private Envelope envelope;

    @Override
    public boolean accept(Key key, Value value) {
        if (key.getColumnFamily().toString().equalsIgnoreCase("MBR")) {
            if (key.getColumnQualifier().toString().equalsIgnoreCase("meta")) {

                WKTReader reader = new WKTReader();
                try {
                    Geometry g = reader.read(value.toString());
                    return g.getEnvelopeInternal().intersects(envelope);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env) throws IOException {
        super.init(source, options, env);
        String[] coordinates = options.get("envelope").trim().split(",");
        Double x1 = new Double(coordinates[0].trim());
        Double y1 = new Double(coordinates[2].trim());
        Double x2 = new Double(coordinates[1].trim());
        Double y2 = new Double(coordinates[3].trim());
        envelope = new Envelope(x1, x2, y1, y2);
    }


    public static void setEnvelope(IteratorSetting si) {

    }
}
