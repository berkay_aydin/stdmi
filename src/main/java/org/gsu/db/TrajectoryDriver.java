package org.gsu.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.accumulo.core.cli.BatchWriterOpts;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MultiTableBatchWriter;
import org.gsu.base.ConfigReader;
import org.gsu.base.DatasetParser;
import org.gsu.base.QueryDatasetParser;
import org.gsu.base.Trajectory;
import org.gsu.base.model.ClassicalModel1;
import org.gsu.base.model.ClassicalModel2;
import org.gsu.base.model.SegmentedStModel;
import org.gsu.base.model.StModel;
import org.gsu.stats.RunStatistics;

/**
 * Hello Java Created by vijay.akkineni on 12/13/14.
 */
public class TrajectoryDriver {

    public static void main(String[] args) throws AccumuloSecurityException, AccumuloException {

        StdmClientOnRequiredTable opts = new StdmClientOnRequiredTable();
        BatchWriterOpts bwOpts = new BatchWriterOpts();
        opts.parseArgs(DbOperationsManager.class.getName(), args, bwOpts);
        Connector connector = opts.getConnector();
        MultiTableBatchWriter mtbw = connector.createMultiTableBatchWriter(bwOpts.getBatchWriterConfig());

        new ConfigReader();
        DatasetParser dp = new DatasetParser();
        System.out.println("Completed reading data files");

        if (opts.model.equalsIgnoreCase("cdm1")) {
            cdm1(connector, dp, opts);
        } else if (opts.model.equalsIgnoreCase("cdm2")) {
            cdm2(connector, dp, opts);
        } else if (opts.model.equalsIgnoreCase("stdm")) {
            stdm(connector, dp, opts);
        } else if (opts.model.equalsIgnoreCase("seg")) {
            segStdm(connector, dp, opts);
        } else if (opts.model.equalsIgnoreCase("nonstdm")) {
            nonstStdm(connector, dp, opts);
        } else if (opts.model.equalsIgnoreCase("nonsegstdm")) {
            nonsegStdm(connector, dp, opts);
        }


        RunStatistics.printDBInsertionTime();
        RunStatistics.printDBMemory();
        RunStatistics.printIndexInsertionTime();
        RunStatistics.printIndexSize();
        RunStatistics.printQuerySearchTime();
        RunStatistics.printHits();
        RunStatistics.printMisses();
        mtbw.close();
    }

    private static void nonsegStdm(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        SegmentedStModel segStdmodel = new SegmentedStModel(connector, "segTrajectory", "", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            segStdmodel.createTables();
            System.out.println("Completed creating tables");
            segStdmodel.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }

        dp.trajectoryMap = null;

        QueryDatasetParser qdp = new QueryDatasetParser();
        Random random = new Random();
        long startTime = System.currentTimeMillis();
        if (opts.queryType.equalsIgnoreCase("nonstsearch")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    String trajectoryID = trajMap.get(randomTrajectory).getTrajectoryId();
                    System.out.print("non st search trajectory ID: " + trajectoryID + " ");
                    boolean isSearchSuccess = segStdmodel.nonSTSearch(trajectoryID);
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;

        // Geometry queryGeom =
        // dp.trajectoryMap.get(8).getGeometryMap().get(22L);
        // System.out.println(queryGeom);
        // segStdmodel.search(dp.trajectoryMap.get(2920), "overlaps",
        // "overlaps");

        // segStdmodel.nonSTSearch("787");
    }

    private static void segStdm(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        SegmentedStModel segStdmodel = new SegmentedStModel(connector, "segTrajectory", "", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            segStdmodel.createTables();
            System.out.println("Completed creating tables");
            segStdmodel.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }

        dp.trajectoryMap = null;

        QueryDatasetParser qdp = new QueryDatasetParser();
        Random random = new Random();
        long startTime = System.currentTimeMillis();
        if (opts.queryType.equalsIgnoreCase("timestamp")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timestamp trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = segStdmodel.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getStartTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("timerange")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timerange trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = segStdmodel.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("swindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial window  trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = segStdmodel.search(trajMap.get(randomTrajectory), "overlaps", "none", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("stwindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial temporal window   trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = segStdmodel.search(trajMap.get(randomTrajectory), "overlaps", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;

        // Geometry queryGeom =
        // dp.trajectoryMap.get(8).getGeometryMap().get(22L);
        // System.out.println(queryGeom);
        // segStdmodel.search(dp.trajectoryMap.get(2920), "overlaps",
        // "overlaps");

        // segStdmodel.nonSTSearch("787");
    }

    private static void nonstStdm(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        StModel stdmodel = new StModel(connector, "stdmTrajectory", "", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            stdmodel.createTables();
            System.out.println("Completed creating tables");
            stdmodel.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }

        dp.trajectoryMap = null;

        QueryDatasetParser qdp = new QueryDatasetParser();
        Random random = new Random();
        long startTime = System.currentTimeMillis();

        if (opts.queryType.equalsIgnoreCase("nonstsearch")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectoryID = keys.get(random.nextInt(keys.size()));
                    boolean isSearchSuccess = stdmodel.nonSTSearch(randomTrajectoryID.toString());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;
    }

    private static void stdm(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        StModel stdmodel = new StModel(connector, "stdmTrajectory", "", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            stdmodel.createTables();
            System.out.println("Completed creating tables");
            stdmodel.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }

        dp.trajectoryMap = null;

        QueryDatasetParser qdp = new QueryDatasetParser();
        Random random = new Random();
        long startTime = System.currentTimeMillis();

        if (opts.queryType.equalsIgnoreCase("timestamp")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timestamp trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = stdmodel.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getStartTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("timerange")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timerange trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = stdmodel.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("swindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial window  trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = stdmodel.search(trajMap.get(randomTrajectory), "overlaps", "none", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("stwindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial temporal window   trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = stdmodel.search(trajMap.get(randomTrajectory), "overlaps", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;

        // Geometry queryGeom =
        // dp.trajectoryMap.get(8).getGeometryMap().get(22L);
        // System.out.println(queryGeom);
        // // stdmodel.search(dp.trajectoryMap.get(8), "overlaps", "overlaps");
        // stdmodel.nonSTSearch("2");
    }

    private static void cdm2(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        ClassicalModel2 cdm2 = new ClassicalModel2(connector, "cdm2Trajectory", "cdm2Meta", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            cdm2.createTables();
            System.out.println("Completed creating tables");
            cdm2.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }
        dp.trajectoryMap = null;

        QueryDatasetParser qdp = new QueryDatasetParser();

        Random random = new Random();
        long startTime = System.currentTimeMillis();

        if (opts.queryType.equalsIgnoreCase("timestamp")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timestamp trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm2.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getStartTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("timerange")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timerange trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm2.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("swindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial window  trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm2.search(trajMap.get(randomTrajectory), "overlaps", "none", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("stwindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial temporal window   trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm2.search(trajMap.get(randomTrajectory), "overlaps", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;

        // Geometry queryGeom =
        // qdp.trajectoryMap.get(2201).getGeometryMap().get(2340L);
        // System.out.println(queryGeom);
        // cdm2.search(qdp.trajectoryMap.get(2201), "overlaps", "overlaps");
    }

    private static void cdm1(Connector connector, DatasetParser dp, StdmClientOnRequiredTable opts) {
        ClassicalModel1 cdm1 = new ClassicalModel1(connector, "cdm1Trajectory", "cdm1Meta", new Boolean(opts.useIndex));

        if (!new Boolean(opts.skipC)) {
            cdm1.createTables();
            System.out.println("Completed creating tables");
            cdm1.populateDatabase(dp.trajectoryMap);
            System.out.println("Completed inserting the data to tables");
        }

        dp.trajectoryMap = null;

        // Spatio Temporal window query
        QueryDatasetParser qdp = new QueryDatasetParser();

        long startTime = System.currentTimeMillis();
        Random random = new Random();

        if (opts.queryType.equalsIgnoreCase("timestamp")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {

                System.out.println("Processing fileName: " + fileName);
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timestamp trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm1.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getStartTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("timerange")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for timerange trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm1.search(trajMap.get(randomTrajectory), "none", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("swindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial window  trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm1.search(trajMap.get(randomTrajectory), "overlaps", "none", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        } else if (opts.queryType.equalsIgnoreCase("stwindow")) {
            for (String fileName : qdp.fileTrajectoryMap.keySet()) {
                Map<Integer, Trajectory> trajMap = qdp.fileTrajectoryMap.get(fileName);
                List<Integer> keys = new ArrayList<Integer>(trajMap.keySet());

                for (int i = 0; i < ConfigReader.getNumberOfQueries(); i++) {
                    Integer randomTrajectory = keys.get(random.nextInt(keys.size()));
                    System.out.print("Searching for spatial temporal window   trajectory: " + trajMap.get(randomTrajectory).getStartTime() + " ");
                    boolean isSearchSuccess = cdm1.search(trajMap.get(randomTrajectory), "overlaps", "overlaps", trajMap.get(randomTrajectory)
                            .getStartTime(), trajMap.get(randomTrajectory).getEndTime());
                    System.out.println("isSearchSuccess = " + isSearchSuccess);
                    if (isSearchSuccess) {
                        RunStatistics.hits += 1;
                    } else {
                        RunStatistics.misses += 1;
                    }
                }
            }
        }

        // cdm1.search(qdp.trajectoryMap.get(1), "overlaps", "overlaps");
        long endTime = System.currentTimeMillis();
        RunStatistics.querySearchTime += endTime - startTime;
    }
}
